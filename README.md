# Haskell Foundation Meeting Minutes

This repo hosts the agendas and minutes from meetings of the Haskell Foundation,
including its Board and various subgroups.
