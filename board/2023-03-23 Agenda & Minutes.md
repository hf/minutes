# Haskell Foundation Board Meeting

23 March 2023


## Agenda

Meeting to start at x:02.

Cancel the next meeting?

Update from ED on ongoing projects:



* Summer of Haskell
* Security advisory DB
* GHC Contributor's Workshop
* Coordinating Nightlies
* Sponsorship Renewals

Private session


## Attendance


## Present

1. David Christiansen
2. Théophile Choutri
3. Ryan Trinkle
4. Tom Ellis
5. Richard Eisenberg
6. Chris Dornan
7. Andrew Lelechenko
8. Edward Kmett
9. Graham Hutton


## Absent



1. Niki Vazou
2. Evie Ciobanu
3. José Pedro Magalhães
4. Simon Peyton Jones
5. Scott Conley


## Minutes


### Cancel the next meeting?

The next planned meeting is on Holy/Maundy Thursday, which is a public holiday for many of us, including the ED. We decided to discuss canceling the meeting on the mailing list. 


### Update from ED on ongoing projects:


#### Summer of Haskell

David: At the last board meeting, it was suggested that I be forbidden from running it. This is technically outside what the board gets to decide, but nonetheless it was rhetorically useful to find another volunteer, so I will not be in charge of it. Summer of Haskell will be run by the Haskell.org Committee, and  the Haskell Foundation will directly sponsor two students. The people with formal responsibilities are Aaron and Ida from the H.org committee. Jasper and Gershom are also involved. There is also space for other people to help out. We need to make sure that the treasurer has the administrative capacity to perform the payouts, and then we should be good to go.

Ryan: I have this capacity.

David: I was asked about the actual impact of Summer of Haskell. We have both attracted now-prominent community members and gotten software that people use regularly out of it, so it seems worth the expense and effort.


#### Security advisory DB

David: We have received a lot of applications. We will have a committee of 5 people with staggered terms of 1 year. I am a little concerned about the fact that it’s all volunteers, but I don’t know a better way to do it.

Richard: What will the volunteers do?

David: They will process incoming advisories. For instance, a vulnerability found in a library would be reported to the author, and a form will be filled out, which will be sent to a well-known contact point. It will be reviewed, make sure it makes sense and added it to the database. The hope is that this process can make Haskell programmers more confident that they aren't  introducing vulnerabilities via dependencies. It will also make it easier for companies that use Haskell to pass an ISO 27001 audit.

Richard: In terms of infrastructure, what does that require of us?

David: It can be a bunch of markdown files with TOML preambles on GitHub, with an email address or PRs for submission.

Richard: Okay. In terms of structure, is there judgment?

David: Yes we will evaluate the validity of each submission.

Richard: And can you fire any of these volunteers?

David: Yes, but I don't expect to ever have to.

Richard: There is a branding opportunity. It would be very cool to have the HF logo and name affixed to it.

David: Agreed, the branding needs to be better. It is now on the TODO list.

David: I focus perhaps too much on doing useful things, rather than being seen to be doing useful things. As a non-profit, we need to both be useful and be visibly useful so we can raise the funds to continue being useful. We should not pretend to be useful, though.

Link to the database: https://github.com/haskell/security-advisories


#### GHC Contributor's Workshop

David: I can open registrations next week for the workshop. We now have 70 people who expressed interest to attend in person. I think we’ll be fully booked.


#### Coordinating Nightlies

David: One ongoing project is coordinating the process of getting more easily-accessible nightly builds of the compiler. The current status is that I have a draft document based on public input about use cases that I’m presenting to key groups for feedback, and it will be posted publicly once I'm more sure of the contents. There was already useful feedback from the GHC team that we need to carefully consider how (or if) the nightlies will be signed, as that's a manual process today. We need to clearly communicate any changed risk profiles.

I’ll also talk to tooling developers and the Stackage admins.

Right now, I'm trying to avoid scope creep and would very much like to only coordinate GHC nightlies to start with, rather than a whole coordinated nightly toolchain, even though that would be useful in the long run.

Question: Why would you want to distribute nightlies of build tools?

David: There are two big arguments for nightlies of Cabal & Stack: 1) it helps you catch incompatibilities in the compiler with the tools ahead of time, and 2) We can catch incompatibilities in development versions of the tools with the compiler. I've also heard that the Cabal team recommends using nightly builds of Cabal with nightly builds of GHC.

Andrew: It is true that in the past a nightly build of Cabal was needed to use pre-releases of GHC but that’s not the case anymore.

David: That's good to hear.


#### Sponsorship Renewals

The ED summarized the state of sponsorship renewals.


#### Other Business

Richard: I wanted to say that there’s a really drumbeat of activities going on, and if we disappeared now there’d be a sucking sound in the community. We hold things together now, and it’s more central now.

We’re getting systemically important in the community and it’s a good feeling.

Chris: I wanted to say thanks for the work on the advisory db, it’s important work.

David: Thank you, my regret is that it’s been put on the back burner too often due to being superseded  by other things. 

Graham: there was a discussion a while ago about changing the sponsorship names to more standard ones (bronze, silver, gold, platinum etc). Was a decision made on this? 

David: I thought about it and decided to shelve that reflection. It’s not super plausible to me that it will make a difference for potential donors. I decided to back off on it because I needed to preserve brain power on that. But we can come back to it. I’ve been told that the environment is brutal these days

Richard: and we’re getting renewals nonetheless! 

David: One thing I’ve been talking with Bryan about is to make an annual report for his 1-year anniversary.

This will be a useful way to highlight what we’re up to for the public.

As far as the vote on the revised policy on financial dealings with Russia, it seems unanimous but I’d like a tally on the vote (The secretary will take care of it).
