Haskell Foundation Board Meeting

9 March 2023


## Agenda

Meeting to start at x:02.

Query from Gabriel Scherer 

Policy on Russia

Private session


## Attendance


## Present


1. David Christiansen
2. Graham Hutton
3. José Pedro Magalhães
4. Richard Eisenberg
5. Simon Peyton Jones
6. Théophile Choutri
7. Tom Ellis
8. Andrew Lelechenko
9. Chris Dornan
10. Scott Conley


## Absent



11. Niki Vazou
12. Evie Ciobanu
13. Ryan Trinkle
14. Edward Kmett


## Minutes


### Query from Gabriel Scherer

David: Gabriel sent an inquiry to the HF Board, for our support to convince the ICFP Steering committee to have very low online participation fees. He wants to know if it’s going to be positive at all, and about the cost itself. There has been some follow-up correspondence, where we talk about numbers. The cost of streaming is around $25k. Both Haskell events would be $4200, assuming a constant rate per room-day..

Simon: My baseline position is to reduce the barrier to entry. A question is “if it was cheaper to attend, would more members of the Haskell community attend?” and we don’t know that. And I need data, and one way to get data is to try that. I think it’s unlikely that there’s a big surplus demand. It wouldn’t be a big priority but worthy of consideration. So we can consider it, but it would be against other data, and different priorities.

Andrew, summarized by David: It could be perceived as rude to be involved in this discussion, as intrusive. 

Richard: From what I know, there has been nothing in the ICFP SC that suggests that anyone has been put off by this proposal.

Tom: I think it would be great for the HF to have a policy for attendance in conferences, and promote diversity and renewal of the speaker pool. It should be decided independently of this particular conversation however. I don’t think the HF would do a lot of good in other conferences.

Andrew: My feeling is that you (Richard & Simon) tend to exaggerate the value of ICFP for the Haskell community. I personally see very little to gain from synchronous participation at this event. In the aftermath of ICFP I’m watching maybe one or two talks, but for the general public there is less value in this content.

Chris: What’s our ED's take on this? 

David: I actually hear a lot of disagreement with what I said in the email thread. My thoughts are that all else equal it would be nice to have more people attending conferences, but all else is not equal, and this is not the kind of place where we can maximize the utilization of our funds and time.

My big limiting factors right now are time and energy to get things done. Hence my preference for not dealing with it. I agree with Andrew here that ICFP is not necessarily the highest-impact venue to lower costs at.

Simon: I think we can say to ICFP SC: 1. We strongly support all ways of broadening participation from under-represented groups at Haskell related conferences. 2. We would therefore welcome the ICFP SC thinking about how to do that. 3. In our judgment, though, if we were to devote resources to this question, we would probably focus on other conferences (by "other conferences" we have in mind more practitioner-oriented ones).

Simon posted a brief reply summary in the comment box, and he and Richard agreed to write the board's email reply along the lines of the summary.


### Policy on Russia

Because Haskell rotated out of GSoC this year, we'd like to organize HSoC again. This means that we need to confront a difficult decision that has thus far been "thunked" due to not actually mattering to our actions: our policy on sending money to Russia. As a US organization, the HF will of course obey all US laws and regulations related to sanctions, but the sectoral nature of the sanctions does not make it clear that it would be necessarily illegal to support students in Russia.

The Executive Director proposed a formulation of a policy for adoption by the board that we not send money into the Russian economy during the war. The proposed policy does not make any discrimination on ethnicity, national origin, or citizenship. In the discussion, the board was in agreement as to the substantive nature of the policy (only suggesting that it, like the GSoC policy, be extended to also cover Belarus and the Russian-controlled regions of Ukraine). There was disagreement related to the specific formulation, and to how it will be communicated. The ED agreed to send an updated text, as well as a communication policy, to the board for a later asynchronous vote after a quick indicative non-binding show of hands about the proposed update.
