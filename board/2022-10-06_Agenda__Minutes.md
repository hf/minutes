# Haskell Foundation Board Meeting
6 October 2022

## Agenda
Meeting to start at x:02.

David: Update from the Executive Director
 * Illness and recovery
 * Haskell Love videos
 * Error message index
 * Web site updates
 * Educator forum
 * ICFP trip report

Private session

## Attendance
### Present
Niki Vazou
Chris Dornan
Tom Ellis
Ryan Trinkle
Graham Hutton
David Christiansen
Simon Peyton Jones
Scott Conley
### Absent
Evie Ciobanu
Andrew Lelechenko
José Pedro Magalhães
Théophile Choutri
Richard Eisenberg
Edward Kmett

## Minutes

Ryan Trinkle as Acting Secretary in the absence of the Secretary and Vice Secretary.

Tom Ellis, Vice Chair, chairing the meeting.

### David: Update from the Executive Director
#### Illness and recovery:
 * David got covid at ICFP; recovering, but still feeling less than fully recovered

#### Haskell Love videos
 * David found someone to do editing; waiting for our counterparty to approve his work

#### Error message index
 * `errors.haskell.org` has been approved by the Haskell.org committee
 * Error code patch merged into GHC main branch

#### Website updates
 * Got spun up on developing the website
 * Projects page has been updated to match reality, "Ideation" section removed
 * Want to hire someone to redo the CSS to simplify versus tailwind so the ED can maintain it

#### Educator forum
 * Good response so far from potential educators
 * Trouble installing toolchain on student computers is one of the big problems identified so far

#### ICFP trip report
 * Met chairman of the board of the Erlang Ecosystem Foundation
 * Met with group who intend to set up an OCaml organization
 * Three presentations:
   * Gave talk on Lean theorem prover at PLMW
   * Gave invited keynote at Haskell Symposium. Goals:
     * Get Haskell researchers interested in working on programming tools
     * Highlight the HF to the eventual Youtube audience
     * Present the HF as an organization that understands researcher and practitioner incentives, enhancing our credibility
   * Demo of how to contribute to error index
 * Discussion themes and topics:
   * The HF plan to write a book about Performance and Optimization is not dead after all! The volunteer is working in a different repository, hosted by his employer.
   * Haddock is very difficult to maintain; maybe we can create a stable interchange format that makes ghc/haddock communication simpler and more stable?
   * It would be useful to connect students to projects that are relatively easy to contribute to that have community mentors, somewhat like GSoC
   * At HIW and Haskell Symposium: lots of things rely on GHC "API", but the API is really just the implementation, and there is no clear boundary between the "official API" and internal stuff.  The plan is to get together with some of these authors to identify desiderata for a subset of GHC functionality that could be put into such an API.
   * Working on GHC is very difficult for various reasons.  David and the board discussed various reasons and potential ways to improve.
   * Talked to HLS developers a lot about how to make their lives easier


