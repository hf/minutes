# Haskell Foundation Board Meeting
3 November 2022

## Agenda
Meeting to start at x:02.

Presentation from Bryan Richter, GHC CI dev, on recent activities, including Q&A (x:02-x:22)

* Update from David on recent activities (x:22-x:30)
  * Haskell Love videos
  * GHC Prioritization Task
  * Building lighter-weight system to track renewals
  * Contacts with haskell.org committee

* Background thoughts from TAB plan? (x:30-x:40)
* Any other business (x:40-x:45)
* Private session (x:45-x:60)

## Attendance
### Present
Ryan Trinkle
José Pedro Magalhães
David Christiansen
Théophile Choutri
Graham Hutton
Richard Eisenberg
Bryan Richter
Tom Ellis
Scott Conley
Simon Peyton Jones
Niki Vazou
Chris Dornan
Evie Ciobanu
Edward Kmett
### Absent
Andrew Lelechenko

## Minutes

### Presentation from Bryan Richter, GHC CI dev, on recent activities, including Q&A

https://docs.google.com/presentation/d/1xQuMXB5CYelHExRxjTW8Id3BoPysBDYXwQbnPd83jDQ/edit#slide=id.p

Richard: Are there aspects of this problem that would become significantly better if we threw money at it, how much money, and is that worth it? You’ve mentioned machines and responsibilities regarding the availability of the services.

Bryan: We rely on a lot of hardware that we don’t control, and to which I don’t have access. I think we need servers that are fully dedicated and not busy running other people’s workload. But they are in-kind donations, and I don’t want to look that gift horse too hard in the mouth. 

Richard: Thank you. It strikes me that if anyone has an extra machine in their attic that they want to use for Haskell stuff, we can probably use these resources for sporadic impact studies. 

Edward: I confess that the notion of piling on random servers makes me nervous. We need to get out of the “Pet” mentality for servers and go toward a “Cattle” strategy.

Ryan: The mac machines would certainly need to be kept as pets, as they are quite peculiar and expensive. But the non-macs could safely be moved to a more cloud-based approach.

Bryan: Something I’d like to do would be to clarify the GHC platform policy and give it more teeth. The platform policy is the different tiers of architectures / platforms that we support. Cf Rust’s own platform policy. I think we should adopt such a thing.

David: This has been roughly 6 months since Bryan started working. Would you like to have some regularity in these presentations?
Richard: Twice a year seems quite good, especially when coupled with the weekly updates. 

David: One last question: Do you think the GHC project is in a position to make a decision about the platform support policy? There isn’t a clear line of authority regarding the implementation (as opposed to Haskell the language implemented by GHC). 

Bryan: I guess not, because it’s quite unclear to whom I should go for such decisions.

Simon: David, there isn’t some sort of obvious group to manage that, and it is my understanding that the HF is in the position (and has the mission) to coordinate those things.

### Update from David on recent activities

#### Haskell Love videos
David: The first batch of Haskell Love videos has been edited, delivered and approved. 

#### GHC Prioritization Task
David: I have sent emails to stakeholders to get information about the usage of GHC and suggestions to the development team.

#### Building lighter-weight system to track renewals
David: The current system is not quite appropriate in terms of complexity to track the renewal of sponsors, so I am building something lighter-weight. For the size of our organisation, we don’t need to track in a big fancy database.

#### Contacts with haskell.org committee
David: I have been in touch with the h.org committee, and I would like to pursue better communications and collaboration with the committee. I have a call with Jasper planned.

### Background thoughts from TAB plan

David: I’d like to ask about the features of the Technical Advisory Board. This is something that Andrew B. had put together, and I was wondering how the seat allocation was to be handled.

Chris: There have been a lot of discussions about that. I have been both worried about the perception of the community, but also sympathetic to the original needs that spawned this conversation. Avoiding accusations of subordination to industrial interests seems to be important.

Simon: Don’t read too much into the document, it’s better if you come up with something that seems appropriate/helpful to you 
