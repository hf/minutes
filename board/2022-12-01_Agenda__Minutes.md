# Haskell Foundation Board Meeting
1 December 2022

## Agenda
Meeting to start at x:02.

Updates on ongoing projects 
 - Haskell Love videos completely finished and released
 - errors.haskell.org and Stack now support each other
 - GHC user short- and medium-term priorities responses
 - Volunteer found for vulnerability database
 - SWG attempting to get GHC proposal 134 (deprecated exports) implemented

ED's vacation plans: 23-30 December (6 days in all)

What projects make up Haskell and what does it mean?

Private session

## Attendance
Present

Graham Hutton  
David Christiansen  
Simon Peyton Jones  
Richard Eisenberg  
José Pedro Magalhães  
Théophile Choutri  
Scott Conley  
Edward Kmett  
Evie Ciobanu  
Ryan Trinkle  
Chris Dornan  

Absent

Tom Ellis  
Niki Vazou  
Andrew Lelechenko  

## Minutes

### Updates on ongoing projects
#### Haskell Love videos completely finished and released
David: It’s all done

#### errors.haskell.org and Stack now support each other
David: Stack has started emitting errors that are in the index.

#### GHC user short- and medium-term priorities responses
David: I am in the process of condensing the feedback we’ve had and working with the GHC team to develop a concrete task list in response. 

#### Volunteer found for vulnerability database
David: I found a volunteer to run the security advisory database project.

#### SWG attempting to get GHC proposal 134 (deprecated exports) implemented
David: We picked this feature out of a survey listing technical features that could be useful because it has an existing accepted GHC proposal, so it's ready to go and can deliver value quickly.

### What projects make up Haskell and what does it mean?

David led a discussion on what counts as "core Haskell tools". He'd like to identify a set of core tooling in order to ensure that core tools have sufficient maintenance resources, to promote increased communication and coordination between their maintainers, and to help organize documentation and direct users to the right tooling. 
