# Haskell Foundation Board Meeting
11 August 2022

## Agenda
Meeting to start at x:02.

David: Update from the Executive Director (x:02-x:10)
Technical Working Group (TWG) recruitment completed
Haskell Love video recruitment
Error codes project
Security advisory DB project
Upcoming ED vacation 17 August - 6 September

Discussion of strategy document (x:10-x:30)

Private session (x:30-x:60)
Attendance
Present
Chris Dornan
David Christiansen
Tom Ellis
Ryan Trinkle
Théophile Choutri
Scott Conley
Evie Ciobanu
Richard Eisenberg
Edward Kmett
Absent
Simon Peyton Jones
José Pedro Magalhães
Niki Vazou
Andrew Lelechenko
Graham Hutton


## Minutes
### Update from the Executive Director

Technical Working Group (TWG) recruitment completed. We have had two students who applied, and we decided to offer them positions as observers in the group in order to help build the knowledge of junior members of the community.

Video editor recruitment: everyone who was recommended to us is booked up. Will try Upwork next.

The error code project will be finished by contracting someone to complete the last kilometre. 

The Security Advisory DB project was submitted as a proposal to the Technical Working Group, and the discussion is going on. https://github.com/haskellfoundation/tech-proposals/pull/37

### Discussion of strategy document  

David: For some time now, I've been working on making a strategy document that can satisfy the requests that I've gotten. But it has become clear that there is no single specification that will satisfy everyone, and I've been spending enough time on writing something that attempts to appeal to everyone that it's beginning to get in the way of the other work to be done. The opportunity cost is beginning to exceed the potential benefits. Thus, I'd like to propose making a bare-bones document, consisting of nothing but bullet points, that serves the important organisational interests that are in play, but won't necessarily satisfy every board member's request. This, together with a detailed plan for the next six months and a vague plan for the six months after that, should allow us to discuss the substance rather than the form and address concrete questions of prioritisation. 

David: From my perspective, the interests to be addressed by a strategy document are:

* Allowing a more clear discussion of tradeoffs between alternatives
* Helping the board to understand how the ED is thinking about the decisions that are to be made
* Build alignment in the organisation around the tasks to be performed
* Communicate what we are doing to the community and sponsors

All but the last can be served by an internal working document, and we should write a second document that effectively communicates for other audiences.

David: One of the usages of the strategy document is that we can fall back on it in case of disagreement.
Two questions to orient our mental models are:

* What are our hypotheses about the world and how do our actions confirm or disconfirm them?
* What are our heuristics for decision making?

My proposal is to write a document with these specific sections, followed by a plan, and then update it quarterly and as needed to match reality. The result will be a page or so of bullet points in a text file.

Ryan: As an organisation we are walking in a fixed direction

Chris: We didn’t give you David a very good brief regarding the expectations of the strategy document, but I like what you have done. However, for presenting externally we must think about what we extract from that document.

Richard: In our last board meeting discussion, there was debate about the strategy and the direction, but not as much for the bullet points, we kinda liked those. However, if fully understanding the broad lines will give us more power regarding their outcomes, we must also act on the concrete points.

Chris: There is power in vagueness as well.

Richard: We could also have alongside the list of things we are doing a list of things that we do not plan on doing. That would help prioritising.

Ryan: That would be valuable, also with an explanation of why such a thing was chosen over something else (without getting too controversial). I don’t think we should strive to achieve full consensus for decisions. We all trust David on this, and should not try and force more decisions through consensus.

Richard: I would like to propose a format for this document: a list of bullet points that we expect to do over the next n months, where each bullet point also has an annotation that says “why” (1-2 sentences). 
As well as a section for things that we are not doing. And from there at some point, we can talk about external communication.

David: I will be sending the document, please do ask questions and send suggestions.
