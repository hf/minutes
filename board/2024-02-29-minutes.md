# Haskell Foundation Board Meeting

29 February 2024


## Agenda

* Sponsorship update
* Meeting with Mercury and future directions with Botan
* Skillsmatter video effort
    * A sufficient number of people signed up to volunteer
    * José is going to use this as way to get Ida (volunteer community manager) started on managing things
* ZuriHac co-located workshop update
* Haskell Certification update
* Private session


## Attendance


### Present



1. Graham Hutton
2. Andres Löh
3. José Calderón
4. Tom Ellis
5. Richard Eisenberg
6. Chris Dornan
7. Simon Peyton Jones
8. Edward Kmett


### Absent



9. Josh Meredith
10. Evie Ciobanu
11. Ryan Trinkle
12. Scott Conley
13. Hazel Weakly
14. Emily Pillmore
15. Andrew Lelechenko


## ED Update


### Sponsorship

José presents some updates about his efforts to renew existing sponsors.


### Meeting with Mercury

José reports on a meeting he had with Mercury about the status and future of the Botan work and about the Haskell Foundation in general.


### Discussion about evangelism

The HF has mostly been looking inward to the Haskell community, but not been looking outward much, trying to get more users into Haskell. José thinks this is difficult for him to do (even though he agrees) because he has to spend a lot of effort on fundraising, which means talking to existing community members. He believes the potential NSF funding could perhaps help here.

Tom thinks this is a vicious circle. If we don’t reach out to new potential sponsors, we’ll never get them. Doing more outreach and growing the community is important. Simon says he’s leaning in the other direction. Whether people adopt Haskell or not is often outside of our control. He would rather focus on making existing users of Haskell happy so that they become evangelists. He is also worried that we could spend quite a lot of resources on this without any benefit. Richard thinks that at any point in time, there are some few people who are shopping for something new. Those people currently all go to Rust, because we are not visible enough. Richard would like to win over these people by doing a little bit of evangelism. Tom thinks programming languages grow because people get things done with it, and it has a lot of applications, and not by making a small number of people even happier. Simon wants to know what we need to do. Tom says better documentation, better onboarding story, no friction with the toolchain (such as HLS not working), no difficult error messages in Cabal. Simon can relate to this. Andres thinks that streamlining the onboarding process is worth quite a bit of effort. Richard thinks we might be able to measure and quantify some of this, which might enable us to observe changes or improvements.


### Skillsmatter videos

Twelve volunteers signed up for processing the videos. Simon suggests giving them public credit right now. Gershom found archived talk descriptions that can be recovered and makes the task easier. Ida, our new volunteer community organizer, will take the coordination of this on. Target is that we get more than ten videos done / released every two weeks. Andres asks how many videos there are. Jose says on the order of 250, but there may be duplicates.


### ZuriHac ecosystem workshop

Will happen. Capacity 70 or even 80 people. Registration about to be opened.


### Serokell / Haskell Certification Update

José is providing an update on the negotiations that he, Josh and Andres had with Serokell about the Haskell Certification effort. It is likely that we will reach some form of agreement soon that can then be brought to the Board for approval. Chris emphasizes how important this effort is in his opinion, and thanks everyone involved.


### Miscellaneous

More work on accounting / budget is happening. 

Tom asks for clarification on the terms of existing Board members. Expiring members of the Board according to Richard are: Scott, Chris, Richard, Ed, Ryan (but all are allowed to re-nominate themselves). Andrew is on the Board until next year.

