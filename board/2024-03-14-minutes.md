# Haskell Foundation Board Meeting

14 March 2024


## Agenda

* Update from ED
    * Sponsor updates 
    * Report from meeting with Galois CEO
    * ED’s thoughts on board expansion
* Private session


## Attendance


### Present



1. Andres Löh
2. José Calderón
3. Scott Conley
4. Tom Ellis
5. Evie Ciobanu
6. Chris Dornan
7. Richard Eisenberg
8. Emily Pillmore
9. Simon Peyton Jones
10. Edward Kmett
11. Hazel Weakly
12. Andrew Lelechenko


### Absent



13. Josh Meredith
14. Ryan Trinkle
15. Graham Hutton


## Minutes


## Sponsorship

José provides a status update on the ongoing conversations with our sponsors. The board also suggests some potential new sponsors José might want to contact.


### Meeting with Galois

José had a meeting with Rob Wiltbank about the HF. There should be some “menu” of tasks (that companies could sponsor) we can publish or circulate. Hazel notes in chat that she’s already been working on such a menu of infrastructure tasks with Bryan. 


### ED thoughts about expanding the board

If there are many motivated people, José thinks it would make sense to expand. Jose has two concerns: diffusion of responsibility, and potentially bikeshedding. He wonders whether the board itself should reform into two layers: one active part concerned with day-to-day affairs, and one less active part concerned with oversight.


### Private session

There was a private session with further discussions about the board size, the roles of board, chair and ED, as well as discussion about the nominations for board membership.

