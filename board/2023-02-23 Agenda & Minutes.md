Haskell Foundation Board Meeting

23 February 2023


## Agenda

Meeting to start at x:02.

Haskell Summer of Code

Administrative Assistant

Projects:



* GHC development workshop @ ZuriHac
* Coordinating nightlies
* Donated web design work
* Security advisory database

Private session


## Attendance


### Present

1. Scott Conley
2. Chris Dornan
3. David Christiansen
4. Graham Hutton
5. José Pedro Magalhães
6. Ryan Trinkle
7. Richard Eisenberg
8. Edward Kmett
9. Tom Ellis
10. Andrew Lelechenko
11. Simon Peyton Jones
12. Théophile Choutri


### Absent

13. Niki Vazou
14. Evie Ciobanu

## Minutes

### Haskell Summer of Code

Google did not accept the Haskell project, so we’ll try to “run our own”. This is unfortunate, but has happened before. Historically, Google has rotated through different organizations, and we shouldn't necessarily read anything into the rejection above and beyond that we were accepted the last few years. In the past, we've rotated out and then in again.

### Administrative Assistant

David has engaged an administrative assistant for help with things such as scheduling meetings. She is working one day a week for us. If you get emails from her HF address, they are not phishing. She works at Flipstone, and had some extra capacity. During discussions with board members about how to recruit someone, Scott mentioned her. Rates are determined by an analysis of rates for similar work on Upwork.

### GHC development workshop @ ZuriHac

Over 100 responses. Next step is to figure out the registration rates, in particular for corporate participants. David could use feedback on this.

This will be a 3-day event, before the actual ZuriHac. The plan is to charge admission for all on-site participants, because space is limited. Students will pay a token placeholder fee (around USD 40), individual professionals a few hundred (comparable to a plane ticket), and corporate rates around typical market rates for a three-day training. Corporate rates would essentially be used to sponsor speaker travel costs, and any leftover money will be used to defray the costs of student participation.


### Coordinating nightlies

Still in progress with David. He has a first draft mostly written, and will attempt to get feedback from the GHC team soon.


### Donated web design work

No update at this stage.


### Security advisory database

There have been applications, and they are being processed by the volunteer running the project.
