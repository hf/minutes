# Haskell Foundation Board Meeting

26 January 2023


## Agenda

Meeting to start at x:02.

It's board member election time!

Project updates


* GHC developer intro @ ZuriHac
* GHC medium-term priorities
* Haskell.org marketing collaboration
* Performance book
* Cross-cutting UX issue coordination
* GHC Nightlies
* Haskell School

Hosting Discourse on behalf of a corporate research group?

Private session (x:45-x:60)


## Attendance


### Present

1. Théophile Choutri
2. David Christiansen
3. Graham Hutton
4. Tom Ellis
5. Richard Eisenberg
6. José Pedro Magalhães
7. Simon Peyton Jones
8. Chris Dornan
9. Ryan Trinkle
10. Andrew Lelechenko
11. Niki Vazou


### Absent

12. Scott Conley
13. Edward Kmett
14. Evie Ciobanu


## Minutes


### Board member election time

A reminder that, according to our [bylaws](https://gitlab.haskell.org/hf/meta/-/blob/main/board.md), an invitation is to be sent by 10-Feb-2023, triggering the yearly application process. The secretary is responsible for this process.


### Project updates (0:40)


#### GHC developer intro @ ZuriHac 

David: Simon & I are preparing a GHC developer intro for next ZuriHac. The idea is to produce introductory material for new contributors as a side-effect of this session.

I sent the ZuriHac organizers an expression of interest form that they will put up. Time was also spent on GDPR compliance. The intention is that during ZuriHac the people who have attended this session would have access to mentors for a first, guided contribution.

Simon: I think it will be packed

Richard: I think it can be successful. The problem that could arise is “what happens afterward?”. I want to make sure that there are open arms ready to receive whatever will come in.


#### GHC medium-term priorities (0:02)

David: Based on the interest shown here and in other fora, I created a public version of the reply from the GHC team. The text was identical, except for an introduction explaining the context and the addition of ticket numbers to the text. Every specific item that the GHC team will work on has had a ticket opened (some of them closed as duplicates). It then led to a very long thread. I think that the discussion suffered from not everyone using words with the same meaning or having enough insight into the GHC process, but I tried to clarify terms and provide background information from time to time. It would be nice to have only well-informed conversations, but the nature of public discourse is that not everyone has the same background.


#### Haskell.org marketing collaboration (0:02)

David: The process is going well - we keep having meetings where we bring up controversial points and find out that everyone already agrees. This is a sign that it's time to move forward. The donor is at a point where they will send a proposal to the Haskell.org committee. Hopefully we will get a much better webpage on haskell.org


#### Performance book (0:05)

David: The project has actually been happening in a different repository by different people. Moving the repo to the HF is currently underway. CI checks of the code are being implemented.

Richard: What is the scope of collaboration? There are lots of techniques and classes of readers out there.

David: The audience is not new haskellers. It will probably be a series of well-contained introductions to various techniques. I think it’s got real potential.


#### Cross-cutting UX issue coordination (0:01)

David: I talked to Julian and he’s not really available to make progress on that, and neither am I. We are putting this project on the shelf for the time being.


#### GHC Nightlies (0:15)

David: From the above forum thread, a discussion happened on using GHC Nightly releases. The tooling ergonomics is not here yet, and the infrastructure needs to be in place to get a collection of nightlies.

The GHC CI process produces meta-data for the build that is required for binary distributions. This would

Enable really valuable things, like the GHC team getting feedback on a change happening on large corporate codebases. Another thing that seems useful would be to have nightlies based on feature branches (that fix a bug). Feedback will be easier to get.

Hécate: As someone working on such codebases, I appreciate this effort. I’d like to raise the fact that this has to go hand-in-hand with minimal breakage from the various groups that are responsible for them, because otherwise adopting / testing a nightly is not a matter of Cabal bounds, but also concrete patches to a certain number of direct dependencies, direct and indirect.

Tom: I think that if the nightly releases lead us to a situation where people can see that they cannot run stuff with a nightly, that’s already a good thing. It will lead to some sort of incentive to make sure that there are less breakages. Just having them will increase visibility. 

David: I encourage you to write usage scenarios in the Discourse thread that I’ll process on behalf of the GHCup and GHC developers. We want to work together to run this process based on concrete user needs rather than our abstractions of users.


#### Haskell School (0:05)

David: Haskell School is a website based on the Elixir School that has tutorials for the Haskell language. We have an existing agreement with a company that can write content. I will pause it because I don’t think we are in a position to get any value from that project. The project currently doesn't build due to the Ruby-based code not having been maintained, and I couldn't get the nix-based builder to work either. This seems problematic for contributors. I don’t think we’re able to make sure that the content doesn’t bitrot, and we need to avoid duplicating existing community efforts by attempting to coordinate with those who have already made good materials.

I don’t have the admin capacity to handle this right now. The prerequisites to continuing the work are CI, community coordination, working with haskell.org to get it a place on the website, and a technical infrastructure that's more robust and doesn't require lots of skill in other languages. Once we have done that, I will see if people from MVS can have a role as editors and authors, rather than just authors. But without a volunteer project manager, it will have to wait for a bit.


### HF hosting a Discourse instance for a corporate research lab

A company that does significant programming language research approached us about running a Discourse instance for their lab. On the one hand, it seems not in our core mission; on the other, this company does use Haskell and has close ties to the Haskell research community and having a good relationship would be nice. If they cover the costs and we figure out a way to make it not take much ED time and we can find a solution for them to moderate it, then it might be worth it. Board members are encouraged to contact the ED with their thoughts, he'll decide within the next week.
