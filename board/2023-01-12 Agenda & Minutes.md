Haskell Foundation Board Meeting

12 January 2023


## Agenda

Project updates :



* Hackage Security
* errors.haskell.org and GHCup
* GHC prioritization
* Tooling UX coordination
* FOSDEM
* Marketing collaboration

Fundraising update

Private session


## Attendance


### Present



1. Théophile Choutri
2. David Christiansen
3. Graham Hutton
4. Tom Ellis
5. Ryan Trinkle
6. Scott Conley
7. Richard Eisenberg
8. Andrew Lelechenko


### Absent



1. Edward Kmett
2. Evie Ciobanu
3. José Pedro Magalhães
4. Chris Dornan
5. Niki Vazou
6. Simon Peyton Jones


## Minutes


### Project updates:


#### Hackage Security

David: I was approached by Gershom last summer because the signing keys used by Hackage were going to expire and it was unclear who was going to email the keyholders to request them to sign the new version. This seemed like a good project for the HF, so I documented and coordinated the process.

This revealed some risks: in particular, some people believed to have keys did not, and others did not respond quickly to emails. This meant that the total number of active keys was closer than we would like to the threshold required for a valid signature.

I wrote up a reform proposal that addresses these concerns and it's been well-received so far by the keyholders. In the new process, keyholders are expected to sign or object to a signature every round, even if the threshold is already met, to ensure that everyone on the list is interested in continuing and still has access to the technical capabilities required. Non-responding keyholders will be replaced by new ones. We are also instituting a formal process for resignation. This seems reasonable, because signing is not a huge demand on people’s time (perhaps 15 minutes annually, including building the signing software, checking the root.json file, and carrying out the signature). In the process I discovered that two supposed key holders did not in fact have keys. 


#### errors.haskell.org and GHCup

David: We now have support from GHCup for the Errors Index. GHCup implemented ANSI codes for hyperlinks so that the error IDs are clickable in many popular terminal emulators, and the error index now sports a 404 page that instructs people who land on missing documentation pages how to contribute or request docs.


#### GHC prioritization

David: I helped the GHC team figure out the next 6 months of development effort through sending a survey to a small number of expert users and tools developers. I spent some time categorizing the results,

And we had a series of meetings to determine the next 6 months.

Hécate: Expressed sincere appreciation for quality of the resulting document and endorsed its ongoing production and noted some “diversity of viewpoints” in the tooling community around perceived value therein.

Bodigrim: Offers general support of the structure and approach.


#### Tooling UX coordination

David: When people who have no easy access to Haskell experts want to try Haskell, they run into situations they don’t understand. For example, in some configurations, building a project from the command line can lead to HLS mysteriously ceasing to work. Expert users have workarounds, but we'd like to eliminate the friction.

The overall Haskell tooling UX is built from the composition of many different tools and it’s not always immediately clear where issues need to be fixed.

We have a case of scattered reports: The VSCode extension would get a ticket but it’s not the responsible party. So I had a long chat with Julian and we are planning to create a venue in which to coordinate fixes to cross-cutting user interface and reliability issues. This is not a bug tracker for general users - we'd like it to be a forum for implementers to describe behavior from incoming bug reports.

The suggested format is in four parts:



1. We describe the issue as observed by the user;
2. Explain why it happens (hypothesis);
3. Given that hypothesis, we suggest a work-around until it can be fixed;
4. Discuss and coordinate fixes to make the workaround unnecessary.

Tom: I think this is great stuff and I’m really glad you’re involved in this.

David: I think this is a big thing to worry about: people who bounce off the language because of such roadblocks in the tooling.

We'll try to seed the repo with a few reports to test out this strategy.


#### FOSDEM

David: I will attend FOSDEM, because:


1. Many other people running open-source foundations will gather there
2. I will give a talk, based on my Haskell Symposium keynote. I will present high-impact opportunities for contributions and plug the HF.


#### Marketing collaboration

David: Meetings with the potential donor of improvements to haskell.org continue and are going well.


## Fundraising update
