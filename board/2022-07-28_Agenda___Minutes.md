﻿# Haskell Foundation Board Meeting
28 July 2022


## Agenda
Meeting to start at x:02.


David: Update on recent projects (x:02-x:07)
* Hackage signature process
* Haskell Love videos
* TWG recruitment


Discussion of draft strategy document (x:07-x:45)
* Suggested 2022 plan


Private session (x:45-x:60)


## Attendance
Present
1. Simon Peyton Jones
2. José Pedro Magalhães
3. David Christiansen
4. Tom Ellis
5. Ryan Trinkle
6. Théophile Choutri
7. Scott Conley
8. Evie Ciobanu
9. Richard Eisenberg
10. Niki Vazou
11. Andrew Lelechenko
12. Edward Kmett

Absent

13. Chris Dornan
14. Graham Hutton


## Minutes
### Update on recent projects
Hackage signature process: completed, new root file deployed. We’ve learned that the process can be improved. David will prepare a proposal to this end.


Haskell Love videos: currently searching for video editors.


TWG recruitment: in progress.

### Draft strategy document
_Open-floor discussion_

David would like us to focus on high-level aspects for now, before going into sentence-level details.
Question to the board: should our mission be focused on serving the community, broadening adoption of the language, or something else?

Hécate: serving the community will boost adoption.

Richard: I’d go more specifically into “broadening commercial adoption”. Other things are in service to that.

Ryan: broadly agree with Richard, but essentially it’s about the product of both.

Simon: our goal is to serve the community, and broadening adoption is one metric.

Tom: Is there anything in our 6-12 month roadmap that would not fit with either of those two missions?

David: Even if not, it’s important to consider the impact of the mission statement.

Tom: should we focus more on actions rather than a one-sentence mission statement?

Andrew: serving the community is a good mission statement, but it’s not a measurable goal.

Simon: "serve the community" is a good mission statement. We can amplify it with "empowering volunteers", "filling the gaps", "doing the boring but useful". 

Richard: we as a board have been delaying this decision (mission) for too long

Ryan: the board can hopefully feel comfortable with leaving this decision to David

Tom: Deciding on a mission in abstract is hard; it would be good to have concrete examples of what might be ruled out or encouraged by different mission statements.

Pedro: even stating that “the Haskell community includes its commercial users”, it excludes all the users who are not yet using Haskell, or those who have used and stopped.

## Suggested 2022 plan

Richard: the document isn’t very concrete. What are the concrete things the HF might do?

David: presents a monthly plan of concrete actions he’s planning to take / oversee for the rest of the year

Andrew: I’m personally very skeptical about splitting base from GHC

David: happy to discuss but possibly at another time

Simon:I was hoping to see something about a small-grants programme for community projects

Richard: how does each of these actions connect to the strategy? Can you make that link explicit?
It sounds like the common theme of all these points is "Professionalising Haskell"

Tom: these concrete problems and actions do make things clearer for me and change the problem from "coming up with a strategy in the abstract" to "summarising pithily a collection of activities that we want to perform". 

Richard: fundraising is not on this list.

David: I see it as something complementary to everything else.

David: are there things on this list that shouldn’t be there? Or things that are missing?

Tom: GHC is very important, but is “contributing to GHC” as important as everything else on this list?

Pedro: would need more time to answer David’s question

Hécate: improving the experience of using GHC feels more important to me than contributing to it.

Ryan: efforts to enable tooling would be good to see

Simon: One more concrete action: GHC.X.Hackage -- as a short title for "make it much easier and faster to get the library ecosystem to update to a new version of GHC"

David: in this list I’m focusing on the things I would be personally responsible for, and I don’t see GHC.X.Hackage as one such thing

Simon: I think it would be good to not think of the list personally, but more as the actions we the HF would like to achieve / empower in the next few months
The mission is not a measurable goal. We need shorter term goals (3-12 months off), regularly updated.
